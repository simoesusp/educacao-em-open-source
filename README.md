## Grupo de Extensão em Open Source (GEOS)

- Póxima reunião com o Grupo de Alunos (Terça, 04-05 as 16h): meet.google.com/atb-kmhn-jer


Link para o Doc com o projeto: https://docs.google.com/document/d/1egUY2wPBXMnWmOuRGXsmffpCI153AJ2vvNjRENWU-js/edit?usp=sharing

Link para o Doc com o resumo do projeto: https://docs.google.com/document/d/1hjREaibCiEqmtdh2oNv1VMIpby3L1bE3jc1bozaxNrg/edit?usp=sharing

Link para uma lista de alunos interessados: https://docs.google.com/document/d/1z2gox02udlyhYJ2rMeSYE4utd2Falml4-9C5LJcv1Z0/edit?usp=sharing

## Educação em Open Source


O Grupo de Extensão em Open Source (GEOS) é um grupo de alunos e professores do Instituto de Ciências Matemáticas e de Computação da Universidade de São Paulo (ICMC/USP), que tem como objetivo promover e apoiar atividades que envolvam a produção de cursos de difusão de tecnologias não proprietárias.

Qualificam-se como Open Source todas as formas de artefatos intelectuais abertos de qualquer natureza que expressam conhecimento científico, tecnológico ou artístico em suas diversas materializações, incluindo quaisquer itens passíveis de proteção de direitos autorais ou propriedade intelectual, disponibilizados publicamente, cuja disponibilização pública garanta condições equitativas e irrestritas de utilização para quaisquer finalidades em em quaisquer condições, modificação indondicional à expressa autorização e distribuição na sua forma original ou modificada, gratuita ou comercialmente. Estes artefatos intelectuais incluem, mas não estão limitados a:


- software computacional, incluindo código fonte, formatos de arquivos, padrões de codificação de dados e projeto de interface; 


- projeto de hardware, incluindo especificações, desenhos esquemáticos, interface de operação e comunicação e desenho industrial; 


- obras literárias, incluindo material didático, textos científicos, manuais técnicos e imagens; 


- recursos multimídia, incluindo arquivos de áudio, vídeo e conteúdo interativo; 


- expressões artísticas, incluindo arte gráfica, musical e plástica; e


- dados de pesquisa, incluindo pacotes de  experimentação, resultados de simulação, diagramas e imagens. 




O GEOS irá oferecer cursos para escolas públicas da cidade e outras unidades da USP de instalação, configuração e operação de sistemas operacionais open source, bem como de ferramentas de programação e edição de software. Também irá oferecer aconselhamento técnico e metodológico para usuários, desenvolvedores e gestores de projetos Open Source, incluindo Organizações e indivíduos, quer sejam da academia, indústria, administração pública ou outros segmentos da sociedade. 

Incluem-se no escopo da atuação do GEOS:

- esclarecimentos sobre direitos autorais, propriedade intelectual, seleção de licenças, procedimentos de publicação, metodologias e ferramentas de gestão, dentre outros aspectos técnicos, legais e gerenciais;

- suporte institucional às atividades de ensino, pesquisa e extensão universitária, bem como àquelas administrativas da Universidade, que envolvam a geração e disseminação de tecnologias Open Source; 

- promoção de parcerias entre Universidade e Indústria, bem como o intercâmbio permanente com organizações promotoras de tecnologias Open Source e a comunidade de usuários dessas tecnologias; e

- atividades de divulgação e promoção do uso de sistemas operacionais e ferramentas computacionais open source em escolas e instituições públicas da região.

- Projeto de sistemas open source de apoio a comunidade e administração pública.


Mais informações em http://ccos.icmc.usp.br

# Workshop
- Trazer as pessoas para se conhecer
- Fazer uma rede de interação e colaboração 
- Preparar para o simpósio
  - Sanca livre
  - Espaço Maker (outras coisas diferentes de sw)
  - 

# Simposium de Open Source
- 
